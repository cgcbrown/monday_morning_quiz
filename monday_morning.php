<!DOCTYPE html>
<html>
<head>
	<title>Monday morning HTML quiz</title>
	<style type="text/css">
		p {
			color:red;
			background-color:blue;
		}
	</style>
</head>
<body style= 'font-family:arial'>
	<h1>Quiz</h1>
	<p>This document is for the Thursday afternoon quiz</p>

	<ol>
		<li>
			Hypertext Markup Language
		</li>
		<li>
			To structure your websites
		</li>
		<li>
			<a href="www.example.com" target="_blank" >Click here</a>	
		</li>
		<li>
			Cascading Style Sheets
		</li>
		<li>
			Creating and linking a style sheet or creating a style within each element.
		</li>	
	</ol>

	classes allow you to create objects easier without the need of copying code. <br>
	They contain data and methods.

	<h3>
		<?php 
		//Section 3
			class Car {
				private $make;
				private $price;
				private $colour;

				public function __construct($make, $price, $colour) {
					$this->make = $make;
					$this->price = $price;
					$this->colour= $colour;
				}

				public function getMake() {
					return $this->make;
				}

				public function getPrice() {
					return $this->price;
				}
			}

			$car = new Car('porsche', '20000', 'black');
			echo $car->getMake();
			echo '<br><br>';

		//Section 3 again
			class oddBag {
				private $total;

				public function __construct() {
				}

				public function sumOfBag() {
					$total = 0;
					for ($i=1; $i < 100; $i +=2) {
						$total += $i;
					}
					$this->total = $total;
					echo $total . '<br>';

					if ($total > 2000) {
						$big = 'BIG';
						echo $big;
					} else if ($total == 2000) {
						$meh = 'meh';
						echo $meh;
					} else {
						$small = 'small';
						echo $small;
					}
				}
	
			}

			$oddBag = new oddBag();
			$oddBag->sumOfBag();

			echo '<br><br>';

			//Section 4
			
		?>
	</h3>
	<h5>
			A schema is a visual representation of a database which consists of tables, procedures and functions. <br><br>

			You can access information from a table in a schema. You can then feed the data into a class to construct an object in your code. <br><br>

			The schema itself will be the showroom. There will be a table which stores the 3 different makes of car and how many of them they have. Each make will have a foreign key that connects it to another unique table for each make which will store data of each model, how many of them there are, how many have sold, there price etc. <br>
			In the schema there will also be a table which stores all the salespeople and the information about them. Also it shall log the ammount of cars that were sold by them. <br>
			There will be also be a table which stores each sold car to log who sold them, how much for and when.
			<br><br>

			The best salesman would be found by totalling up the amount of cars sold by each salesman in that given month or maybe by totalling up the price each car sold for. The one who made the most money is the best. <br>
			The most successful car will be the one that sold the most. This will be find by accessing the sold car database and seeing how many were logged for the given month. <br>
			The most successful model will be found by accessing the databse and finding which model sold the most.

	</h5>





	 	
	 

</body>
</html>